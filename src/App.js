import logo from './logo.svg';
import './App.css';
import React from 'react';
import ReactDOM from 'react-dom';


//import React, { useState } from "react";


var text = '["hello","howAreYou","whatIsYourName"]';
var myArr = JSON.parse(text);
 
class ListOfQuestions extends React.Component {
  state = {
  questions:myArr, number:['']
  }

renderTableData() {
      return this.state.questions.map((question, index) => {
         const name  = question //destructuring
         return (
            <tr key={name}>
               <td>{name}</td>
            </tr>
			
         )
      })
   }

   
  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
	//let number = [...this.state.number]
	//number[i] = i
	

    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
	
	//let number = [
    //  ...this.state.number.slice(0, i),
      //...this.state.number.slice(i + 1)
    //]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
	//let number = this.state.number.concat([''])

    this.setState({
      questions
    })
  }

  render() {
    return (
      <p1>
        {this.state.questions.map((question, index) => (
          <tr>
		  <span key={index}>
            <input
              type="text"
              onChange={this.handleText(index)}
              value={question}
            />
			
            <button onClick={this.handleDelete(index)}>X</button>
			</span>
			</tr>
        ))}
        <button onClick={this.addQuestion}>Add New Question</button>
		
	  <h1>
	  {this.renderTableData()}
	  </h1>
	  </p1>
    )
  }
}


 
export default ListOfQuestions;
